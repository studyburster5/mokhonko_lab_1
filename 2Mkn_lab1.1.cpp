#include <time.h>
#include <iostream>
#include <windows.h>
#include <math.h>

using namespace std;

int cpv(double *v, int nv)
{
	int i;
	for(i=0; i<nv; i++)
	{
		v[i] = (rand()%100-50) / (10.0*(rand()%19+1));
		printf("%5.2f  ", v[i]);
	}
	cout<<"\n";
	return 0;
}

double minv(double *v, int nv)
{
	double mv;
	int i;
	mv = v[0];
	for(i=1; i<nv; i++)
	{
		if(mv>v[i])
		{
			mv=v[i];
		}
	}
	return mv;
}

double F1(double a, double b, double c, const double fD, double g)
{
	double F;
	F = (exp(a)) + fD * g * (b + c);
	return F;
}

int main()

{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const double D=2.11;
	int i, ig, n1, n2, n3;
	srand(time(NULL));
	cout<<" ������ ���������� 3-�� ������ : ";
	cin>>n1>>n2>>n3;
	cout<<" ������ ������� g = ";
	cin>>ig;
	double A[n1], B[n2], C[n3];
	cout<<" ����������� ����� A["<<n1<<"] = ";
	cpv(A, n1);
	cout<<" ����������� ����� B["<<n2<<"] = ";
	cpv(B, n2);
	cout<<" ����������� ����� C["<<n3<<"] = ";
	cpv(C, n3);
	double F;
	F = F1(minv(A, n1), minv(B, n2), minv(C, n3), D, ig);
	printf(" Min ������ A = %5.2f\n", minv(A, n1));
	printf(" Min ������ B = %5.2f\n", minv(B, n2));
	printf(" Min ������ C = %5.2f\n", minv(C, n3));
	printf(" F = %7.5f\n", F);
	system("pause");
	return 0;
}

