#include <time.h>
#include <iostream>
#include <windows.h>
#include <fstream>

using namespace std;

void pv(int *v, int nv, int mv)
{
	int i, j;
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			printf("%4d", *(v+i*mv+j));
		}
		cout<<"\n";
	}
}

void gv(int *v, int nv, int mv)
{
	int i, j;
	for (i = 0; i < nv; i ++)
	{
		for (j = 0; j < mv; j ++)
		{
			*(v+i*mv+j) = rand()%100-50;
		}
	}
}

void sv(int *v, int nv, int mv)
{
	int i, j, is, js;
	js=-1;
	int newv[nv][nv];
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			newv[i][j]=0;
		}
	}
	for(j = 0; j < mv; j ++)
	{
		js=-1;
		for(i = 0; i < nv; i ++)
    	{
			if(*(v+i*mv+j) < 0)
			{
				js++;
				newv[js][j] = *(v+i*mv+j);
			}
		}
		for(i = 0; i < nv; i ++)
		{
			if((*(v+i*mv+j) >= 0))
			{
				js++;
				newv[js][j] = *(v+i*mv+j);
			}
		}
    }
	for(i = 0; i < nv; i ++)
	{
		for(j = 0; j < mv; j ++)
		{
			*(v+i*mv+j)=newv[i][j];
		}
	}
}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j, n, m;
	srand(time(NULL));
	fstream f1; //f1 - ������� ����
	FILE *f2; //f2 - �������� ����
	char sf1[40], sf2[40];// ����� ��� ������� ����� �����
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1.open(sf1); //��������� ���� ��� ���������� � ���������� �����
	if(!f1)
	{
	printf("������� ������� ��� ������� ����� \n");
	return 0;
	}
	f1 >> n;
	f1 >> m;
	printf("������ ��'� ����� ��� ����������: ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	//���� ������� ������� �� ������������� ���� ���������, �� ��������� ���� ������������ � ������� ���� � �� ���������� �� �����
	if(f2== NULL)
	{
	printf("������� ������� ��� ������� ����� \n");
	return 0;
	}
	int A[n][m], B[n][m];
	printf("\n ������ ������� A(%d, %d):\n", n, m);
	gv(&A[0][0], n, m);
	pv(&A[0][0], n, m);
	sv(&A[0][0], n, m);
	printf("\n ����������� ������� A(%d, %d):\n", n, m);  
	pv(&A[0][0], n, m);
	system("pause");
	return 0;
}
